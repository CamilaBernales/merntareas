const Tarea = require('../models/Tarea')
const { validationResult } = require('express-validator');


exports.crearTarea = async (req, res, next) => {


    const errores = validationResult(req);
    if (!errores.isEmpty()) {
        return res.status(400).json({ errores: errores.array() })
    }

    const { nombre, etiqueta } = req.body;

    try {
        let tarea = await Tarea.findOne({ nombre, etiqueta })

        if (tarea) {
            return res.status(400).json({ mensaje: 'Esta tarea ya existe' })
        }

        tarea = new Tarea(req.body);
        await tarea.save();
        res.json({ mensaje: 'Se creo una tarea' })
      
    } catch (error) {
        console.log(error);
        next();
    }

}