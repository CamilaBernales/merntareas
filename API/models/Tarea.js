const mongoose = require('mongoose');

const TareaSchema = mongoose.Schema({
    
    nombre:{
        type:String,
        required: true,
        trim: true
    },
    etiqueta:{
        type:String,
        required: true,
        trim: true
    },
    finalizada:{
        type: Boolean,
        required: true,
        default: false
    },
    created_at:{
        type: Date,
        default: Date.now()
    },
    updated_at:{
        type: Date,
        default: Date.now()
    }

});

module.exports = mongoose.model('Tarea', TareaSchema);