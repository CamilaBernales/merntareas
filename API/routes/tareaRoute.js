const express = require('express');
const router = express.Router();
const tareaController = require('../controllers/tareaController');

const { check } = require('express-validator');


router.post('/tareas',
[
    check('nombre', 'El titulo es obligatorio').not().isEmpty(),
    check('etiqueta', 'La etiqueta es obligatorio').not().isEmpty()
],
tareaController.crearTarea);

module.exports = router;
