const express = require('express');
const morgan = require('morgan');
const mongoose = require('./database');


const app = express();

// Middlewares
app.use(morgan('dev'))
app.use(express.json()); // for parsing application/json
app.use(express.urlencoded({ extended: true})); // for parsing application/x-www-form-urlencoded



// Puerto de la App
const PORT = process.env.PORT || 4000;

// Rutas
app.use('/api', require('./routes/tareaRoute'));
app.use('/api', require('./routes/usuarioRoute'));

app.listen(PORT, ()=>{
    console.log("Servidor funcionado");
})